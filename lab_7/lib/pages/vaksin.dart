import 'package:flutter/material.dart';
import './input_vaksin.dart';

final data = [
  {
    "nama": "Joya Ruth Amanda",
    "tanggalLahir": "23 Desember 2001",
    "nik": "123456789",
    "vaksinKe": "2",
    "tanggalVaksin": "25 Desember 2021",
    "lokasiVaksin": "Jakarta",
  }
];

class Vaksin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Vaksin List')),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.grey[600],
        child: Text('+', style: TextStyle(fontSize: 35)),
        onPressed: () {
          Navigator.of(context)
          .push(MaterialPageRoute(builder: (context) => InputVaksin()));
        },
      ),
      body: Container(
          margin: EdgeInsets.all(10),
          child: ListView.builder(
            itemCount: data.length,
            itemBuilder: (context, i) {
              return Card(
                elevation: 8,
                child: ListTile(
                  title: Text(data[i]["nama"].toString()),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tanggal Lahir    : ${data[i]["tanggalLahir"].toString()}"),
                      Text("NIK                      : ${data[i]["nik"].toString()}"),
                      Text("Tanggal Vaksin : ${data[i]["tanggalVaksin"].toString()}"),
                      Text("Lokasi Vaksin   : ${data[i]["lokasiVaksin"].toString()}"),
                    ],
                  ),
                  trailing:
                      Text("Vaksin ke    :  ${data[i]["vaksinKe"].toString()}"),
                ),
              );
            },
          )),
    );
  }
}
