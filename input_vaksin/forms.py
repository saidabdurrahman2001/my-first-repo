from django import forms
from .models import Vaksin

class VaksinForm(forms.ModelForm):
	class Meta:
		model = Vaksin
		fields = "__all__"
	error_messages = {
		'required' : 'Please Type'
	}
	