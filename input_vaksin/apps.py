from django.apps import AppConfig


class InputVaksinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'input_vaksin'
