from django.shortcuts import render
from .models import Vaksin
from .forms import VaksinForm
from django.http.response import HttpResponseRedirect

# Create your views here.

def index(request):
    vaksin = Vaksin.objects.all().values()  # TODO Implement this
    response = {'vaksin': vaksin}
    return render(request, 'vaksin_index.html', response)

def add_vaksin(request):
    form = VaksinForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return HttpResponseRedirect('/input')
    return render(request, "vaksin_form.html", {'form':form})
    